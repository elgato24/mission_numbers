import datetime

def create_mission_number(old_mission_number):

    #Gather all the information we neeed
    #print(old_mission_number)
    last_mission_number = str(old_mission_number)

    #split last mission number into pieces
    lmn_pieces = last_mission_number.split('-')

    #Get Current Date
    today = datetime.date.today()

    #Get Current Year
    current_year = today.year
    #current_year = 2022

    #Compare Years
    if (int(current_year) > int(lmn_pieces[1])):     
        lmn_pieces[2] = 1
        lmn_pieces[2] = str(lmn_pieces[2]).zfill(5)
        lmn_pieces[1] = str(current_year)
        new_mission_number = lmn_pieces[0]+"-"+lmn_pieces[1]+"-"+lmn_pieces[2]
   
    else:
        lmn_pieces[2] = int(lmn_pieces[2]) + 1
        lmn_pieces[2] = str(lmn_pieces[2]).zfill(5)
        new_mission_number = lmn_pieces[0]+"-"+lmn_pieces[1]+"-"+lmn_pieces[2]
        
    #Return results
    return new_mission_number

    