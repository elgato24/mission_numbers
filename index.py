from create_mission import create_mission_number
import datetime
import sqlite3

#Get Current Date
today = datetime.date.today()

#Get Current Year
current_year = str(today.year)
#current_year = "2022"

con = sqlite3.connect("MissionNumber.db")

cur = con.cursor()

old_mission_number = cur.execute("SELECT m_number from MissionNumbers where m_number like '%" + current_year + "%' ORDER BY m_number DESC").fetchone()[0]

con.close()

new_mission_number = create_mission_number(old_mission_number)

print(new_mission_number)

con = sqlite3.connect("MissionNumber.db")
cur = con.cursor()
cur.execute("Insert into MissionNumbers (m_number) values ('"+ new_mission_number +"')")
con.commit()
con.close()