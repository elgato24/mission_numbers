<?php

function create_mission()
{
    //Get last mission number
    $last_mission_number = "TOU-2022-00004";

    ////Separate last mission number to pieces
    $lmn_pieces = explode("-", $last_mission_number);

    //Get Current Year
    $current_year = date("Y");
    //$current_year = 2023;

    //check if Current year is larger than last mission year 
    if(intval($current_year) > intval($lmn_pieces[1]))
    {
        $lmn_pieces[2] = "1";
        $new_mission_number = $lmn_pieces[0]."-".$current_year."-".sprintf('%05d',$lmn_pieces[2]);
    }
    else
    {
        $lmn_pieces[2] = intval($lmn_pieces[2]) + 1;
        $new_mission_number = $lmn_pieces[0]."-".$lmn_pieces[1]."-".sprintf('%05d',$lmn_pieces[2]);
    }

    //return new mission number

    return print($new_mission_number);
}

create_mission();

?>